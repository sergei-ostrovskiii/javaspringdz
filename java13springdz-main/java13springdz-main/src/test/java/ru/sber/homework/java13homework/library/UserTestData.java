package ru.sber.homework.java13homework.library;

import ru.sber.homework.java13homework.library.dto.RoleDTO;
import ru.sber.homework.java13homework.library.dto.UsersDTO;
import ru.sber.homework.java13homework.library.model.Role;
import ru.sber.homework.java13homework.library.model.Users;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

public interface UserTestData {
    
    UsersDTO USER_DTO = new UsersDTO("login",
                                   "password",
                                   "email",
                                   "birthDate",
                                   "firstName",
                                   "lastName",
                                   "middleName",
                                   "phone",
                                   "address",
                                   new RoleDTO(),
                                   "changePasswordToken",
                                   new HashSet<>(),
                                   false
    );
    
    List<UsersDTO> USER_DTO_LIST = List.of(USER_DTO);
    
    Users USER = new Users("login",
                         "password",
                         "email",
                         LocalDate.now(),
                         "firstName",
                         "lastName",
                         "middleName",
                         "phone",
                         "address",
                         "changePasswordToken",
                         new Role(),
                         new HashSet<>()
    );
    
    List<Users> USER_LIST = List.of(USER);
}
