package ru.sber.homework.java13homework.library;

import ru.sber.homework.java13homework.library.dto.DirectorsDTO;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.dto.FilmsWithDirectorsDTO;
import ru.sber.homework.java13homework.library.model.Films;
import ru.sber.homework.java13homework.library.model.Genre;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface FilmTestData {
    FilmsDTO BOOK_DTO_1 = new FilmsDTO(
            "title1",
            "premierYear1",
            "country1",
            1,
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>(),
            false);

    FilmsDTO BOOK_DTO_2 = new FilmsDTO("filmTitle2",
            "publishDate2",
            2,
            2,
            "storagePlace2",
            "onlineCopyPath2",
            "publish2",
            "description2",
            Genre.NOVEL,
            new HashSet<>(),
            false);

    List<FilmsDTO> BOOK_DTO_LIST = Arrays.asList(BOOK_DTO_1, BOOK_DTO_2);

    Films FILM_1 = new Films("filmTitle1",
            LocalDate.now(),
            1,
            1,
            "publish1",
            "storagePlace1",
            "onlineCopyPath1",
            "description",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>());
    Films FILM_2 = new Films("filmTitle2",
            LocalDate.now(),
            2,
            2,
            "publish2",
            "storagePlace2",
            "onlineCopyPath2",
            "description2",
            Genre.NOVEL,
            new HashSet<>(),
            new HashSet<>());

    List<Films> FILM_LIST = Arrays.asList(FILM_1, FILM_2);

    Set<DirectorsDTO> AUTHORS = new HashSet<>(DirectorsTestData.AUTHOR_DTO_LIST);
    FilmsWithDirectorsDTO BOOK_WITH_AUTHORS_DTO_1 = new FilmsWithDirectorsDTO(FILM_1, AUTHORS);
    FilmsWithDirectorsDTO BOOK_WITH_AUTHORS_DTO_2 = new FilmsWithDirectorsDTO(FILM_2, AUTHORS);

    List<FilmsWithDirectorsDTO> BOOK_WITH_AUTHORS_DTO_LIST = Arrays.asList(BOOK_WITH_AUTHORS_DTO_1, BOOK_WITH_AUTHORS_DTO_2);
}
