package ru.sber.homework.java13homework.library.service;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import ru.sber.homework.java13homework.library.DirectorsTestData;
import ru.sber.homework.java13homework.library.dto.AddFilmsDTO;
import ru.sber.homework.java13homework.library.dto.DirectorsDTO;
import ru.sber.homework.java13homework.library.exception.MyDeleteException;
import ru.sber.homework.java13homework.library.mapper.DirectorsMapper;
import ru.sber.homework.java13homework.library.model.Directors;
import ru.sber.homework.java13homework.library.repository.DirectorsRepository;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class DirectorsServiceTest
      extends GenericTest<Directors, DirectorsDTO> {
    
    public DirectorsServiceTest() {
        super();
        FilmsService filmService = Mockito.mock(FilmsService.class);
        repository = Mockito.mock(DirectorsRepository.class);
        mapper = Mockito.mock(DirectorsMapper.class);
        service = new DirectorsService((DirectorsRepository) repository, (DirectorsMapper) mapper, filmService);
    }
    
    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(DirectorsTestData.DIRECTOR_LIST);
        Mockito.when(mapper.toDTOs(DirectorsTestData.DIRECTOR_LIST)).thenReturn(DirectorsTestData.AUTHOR_DTO_LIST);
        List<DirectorsDTO> directorDTOS = service.listAll();
        log.info("Testing getAll(): " + directorDTOS);
        assertEquals(DirectorsTestData.DIRECTOR_LIST.size(), directorDTOS.size());
    }
    
    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorsTestData.DIRECTOR_1));
        Mockito.when(mapper.toDTO(DirectorsTestData.DIRECTOR_1)).thenReturn(DirectorsTestData.AUTHOR_DTO_1);
        DirectorsDTO directorDTO = service.getOne(1L);
        log.info("Testing getOne(): " + directorDTO);
        assertEquals(DirectorsTestData.AUTHOR_DTO_1, directorDTO);
    }
    
    @Order(3)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(DirectorsTestData.AUTHOR_DTO_1)).thenReturn(DirectorsTestData.DIRECTOR_1);
        Mockito.when(mapper.toDTO(DirectorsTestData.DIRECTOR_1)).thenReturn(DirectorsTestData.AUTHOR_DTO_1);
        Mockito.when(repository.save(DirectorsTestData.DIRECTOR_1)).thenReturn(DirectorsTestData.DIRECTOR_1);
        DirectorsDTO directorDTO = service.create(DirectorsTestData.AUTHOR_DTO_1);
        log.info("Testing create(): " + directorDTO);
        assertEquals(DirectorsTestData.AUTHOR_DTO_1, directorDTO);
    }
    
    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(DirectorsTestData.AUTHOR_DTO_1)).thenReturn(DirectorsTestData.DIRECTOR_1);
        Mockito.when(mapper.toDTO(DirectorsTestData.DIRECTOR_1)).thenReturn(DirectorsTestData.AUTHOR_DTO_1);
        Mockito.when(repository.save(DirectorsTestData.DIRECTOR_1)).thenReturn(DirectorsTestData.DIRECTOR_1);
        DirectorsDTO directorDTO = service.update(DirectorsTestData.AUTHOR_DTO_1);
        log.info("Testing update(): " + directorDTO);
        assertEquals(DirectorsTestData.AUTHOR_DTO_1, directorDTO);
    }
    
    @Order(5)
    @Test
    @Override
    protected void delete() throws MyDeleteException {
        Mockito.when(((DirectorsRepository) repository).checkDirectorsForDeletion(1L)).thenReturn(true);
//        Mockito.when(authorRepository.checkAuthorForDeletion(2L)).thenReturn(false);
        Mockito.when(repository.save(DirectorsTestData.DIRECTOR_1)).thenReturn(DirectorsTestData.DIRECTOR_1);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorsTestData.DIRECTOR_1));
        log.info("Testing delete() before: " + DirectorsTestData.DIRECTOR_1.isDeleted());
        service.deleteSoft(1L);
        log.info("Testing delete() after: " + DirectorsTestData.DIRECTOR_1.isDeleted());
        assertTrue(DirectorsTestData.DIRECTOR_1.isDeleted());
    }
    
    @Order(6)
    @Test
    @Override
    protected void restore() {
        DirectorsTestData.DIRECTOR_3.setDeleted(true);
        Mockito.when(repository.save(DirectorsTestData.DIRECTOR_3)).thenReturn(DirectorsTestData.DIRECTOR_3);
        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(DirectorsTestData.DIRECTOR_3));
        log.info("Testing restore() before: " + DirectorsTestData.DIRECTOR_3.isDeleted());
        ((DirectorsService) service).restore(3L);
        log.info("Testing restore() after: " + DirectorsTestData.DIRECTOR_3.isDeleted());
        assertFalse(DirectorsTestData.DIRECTOR_3.isDeleted());
    }
    
    @Order(7)
    @Test
    void searchDirectors() {
        PageRequest pageRequest = PageRequest.of(1, 10, Sort.by(Sort.Direction.ASC, "directorFio"));
        Mockito.when(((DirectorsRepository) repository).findAllByDirectorsFIOContainsIgnoreCaseAndIsDeletedFalse("directorFio1", pageRequest))
              .thenReturn(new PageImpl<>(DirectorsTestData.DIRECTOR_LIST));
        Mockito.when(mapper.toDTOs(DirectorsTestData.DIRECTOR_LIST)).thenReturn(DirectorsTestData.AUTHOR_DTO_LIST);
        Page<DirectorsDTO> directorDTOS = ((DirectorsService) service).searchDirectors("directorFio1", pageRequest);
        log.info("Testing searchAuthors(): " + directorDTOS);
        assertEquals(DirectorsTestData.AUTHOR_DTO_LIST, directorDTOS.getContent());
    }
    
    @Order(8)
    @Test
    void addBook() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorsTestData.DIRECTOR_1));
        Mockito.when(service.getOne(1L)).thenReturn(DirectorsTestData.AUTHOR_DTO_1);
        Mockito.when(repository.save(DirectorsTestData.DIRECTOR_1)).thenReturn(DirectorsTestData.DIRECTOR_1);
        ((DirectorsService) service).addFilms(new AddFilmsDTO(1L, 1L));
        log.info("Testing addFilm(): " + DirectorsTestData.AUTHOR_DTO_1.getFilmsId());
        assertTrue(DirectorsTestData.AUTHOR_DTO_1.getFilmsId().size() >= 1);
    }
    
    @Order(9)
    @Test
    protected void getAllNotDeleted() {
        DirectorsTestData.DIRECTOR_3.setDeleted(true);
        List<Directors> directors = DirectorsTestData.DIRECTOR_LIST.stream().filter(Predicate.not(Directors::isDeleted)).toList();
        Mockito.when(repository.findAllByIsDeletedFalse()).thenReturn(directors);
        Mockito.when(mapper.toDTOs(directors)).thenReturn(
              DirectorsTestData.AUTHOR_DTO_LIST.stream().filter(Predicate.not(DirectorsDTO::isDeleted)).toList());
        List<DirectorsDTO> directorDTOS = service.listAllNotDeleted();
        log.info("Testing getAllNotDeleted(): " + directorDTOS);
        assertEquals(directors.size(), directorDTOS.size());
    }
    
}
