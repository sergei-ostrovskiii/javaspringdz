package ru.sber.homework.java13homework.library;



import ru.sber.homework.java13homework.library.dto.OrdersDTO;
import ru.sber.homework.java13homework.library.model.Orders;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderTestData {
    
    OrdersDTO BOOK_RENT_INFO_DTO = new OrdersDTO(LocalDateTime.now(),
                                                             LocalDateTime.now(),
                                                             false,
                                                             14,
                                                             1L,
                                                             1L,
                                                             null);
    
    List<OrdersDTO> BOOK_RENT_INFO_DTO_LIST = List.of(BOOK_RENT_INFO_DTO);
    
    Orders BOOK_RENT_INFO = new Orders(null,
                                                   null,
                                                   LocalDateTime.now(),
                                                   LocalDateTime.now(),
                                                   false,
                                                   14);
    
    List<Orders> BOOK_RENT_INFO_LIST = List.of(BOOK_RENT_INFO);
}
