package ru.sber.homework.java13homework.library.service;

import org.junit.jupiter.api.Test;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.exception.MyDeleteException;
import ru.sber.homework.java13homework.library.model.Films;

public class FilmServiceTest extends GenericTest<Films, FilmsDTO> {
    
    @Test
    @Override
    protected void getAll() {
    
    }
    
    @Test
    @Override
    protected void getOne() {
    
    }
    
    @Test
    @Override
    protected void create() {
    
    }
    
    @Test
    @Override
    protected void update() {
    
    }
    
    @Test
    @Override
    protected void delete() throws MyDeleteException {
    
    }
    
    @Test
    @Override
    protected void restore() {
    
    }
    
    @Test
    @Override
    protected void getAllNotDeleted() {
    
    }
}
