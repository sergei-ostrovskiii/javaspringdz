package ru.sber.homework.java13homework.library;

import ru.sber.homework.java13homework.library.dto.DirectorsDTO;
import ru.sber.homework.java13homework.library.model.Directors;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public interface DirectorsTestData {
    DirectorsDTO AUTHOR_DTO_1 = new DirectorsDTO("directorFio1",
                                           "birthDate1",
                                           "description1",
                                           new HashSet<>(),
                                           false);
    
    DirectorsDTO AUTHOR_DTO_2 = new DirectorsDTO("directorFio2",
                                           "birthDate2",
                                           "description2",
                                           new HashSet<>(),
                                           false);
    
    DirectorsDTO AUTHOR_DTO_3_DELETED = new DirectorsDTO("directorFio3",
                                                   "birthDate3",
                                                   "description3",
                                                   new HashSet<>(),
                                                   true);
    
    List<DirectorsDTO> AUTHOR_DTO_LIST = Arrays.asList(AUTHOR_DTO_1, AUTHOR_DTO_2, AUTHOR_DTO_3_DELETED);
    
    
    Directors DIRECTOR_1 = new Directors("director1",
                                 LocalDate.now(),
                                 "description1",
                                 null);
    
    Directors DIRECTOR_2 = new Directors("director2",
                                 LocalDate.now(),
                                 "description2",
                                 null);
    
    Directors DIRECTOR_3 = new Directors("director3",
                                 LocalDate.now(),
                                 "description3",
                                 null);
    
    List<Directors> DIRECTOR_LIST = Arrays.asList(DIRECTOR_1, DIRECTOR_2, DIRECTOR_3);
}
