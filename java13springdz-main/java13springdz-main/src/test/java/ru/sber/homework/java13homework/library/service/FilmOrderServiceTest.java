package ru.sber.homework.java13homework.library.service;

import org.junit.jupiter.api.Test;
import ru.sber.homework.java13homework.library.dto.OrdersDTO;
import ru.sber.homework.java13homework.library.exception.MyDeleteException;
import ru.sber.homework.java13homework.library.model.Orders;
import ru.sber.homework.java13homework.library.service.GenericTest;

public class FilmOrderServiceTest extends GenericTest<Orders, OrdersDTO> {
    @Test
    @Override
    protected void getAll() {
    
    }
    
    @Test
    @Override
    protected void getOne() {
    
    }
    
    @Test
    @Override
    protected void create() {
    
    }
    
    @Test
    @Override
    protected void update() {
    
    }
    
    @Test
    @Override
    protected void delete() throws MyDeleteException {
    
    }
    
    @Override
    protected void restore() {
    
    }
    
    @Test
    @Override
    protected void getAllNotDeleted() {
    
    }
}
