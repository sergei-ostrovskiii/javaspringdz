//package ru.sber.homework.java13homework.library.mapper;
//
//import jakarta.annotation.PostConstruct;
//import org.modelmapper.ModelMapper;
//import org.springframework.stereotype.Component;
//import ru.sber.homework.java13homework.library.dto.DirectorsWithFilmDTO;
//import ru.sber.homework.java13homework.library.model.Directors;
//import ru.sber.homework.java13homework.library.model.GenericModel;
//import ru.sber.homework.java13homework.library.repository.FilmsRepository;
//
//import java.util.HashSet;
//import java.util.Objects;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//@Component
//public class DirectorsWithFilmMapper extends GenericMapper<Directors, DirectorsWithFilmDTO> {
//
//    private final FilmsRepository filmsRepository;
//
//    protected DirectorsWithFilmMapper(ModelMapper mapper,
//                                      FilmsRepository filmsRepository
//    ) {
//        super(mapper, Directors.class, DirectorsWithFilmDTO.class);
//        this.filmsRepository = filmsRepository;
//    }
//
//
//    @PostConstruct
//    protected void setupMapper() {
//        modelMapper.createTypeMap(Directors.class, DirectorsWithFilmDTO.class)
//                .addMappings(m -> m.skip(DirectorsWithFilmDTO::setFilmsId)).setPostConverter(toDTOConverter());
//
//        modelMapper.createTypeMap(DirectorsWithFilmDTO.class, Directors.class)
//                .addMappings(m -> m.skip(Directors::setFilms)).setPostConverter(toEntityConverter());
//    }
//
//    @Override
//    protected void mapSpecificFields(DirectorsWithFilmDTO source, Directors destination) {
//        destination.setFilms(new HashSet<>(filmsRepository.findAllById(source.getFilmsId())));
//    }
//
//    @Override
//    protected void mapSpecificFields(Directors source, DirectorsWithFilmDTO destination) {
//        destination.setFilmsId(getIds(source));
//    }
//
//    @Override
//    protected Set<Long> getIds(Directors entity) {
//        return Objects.isNull(entity) || Objects.isNull(entity.getId())
//                ? null
//                : entity.getFilms().stream()
//                .map(GenericModel::getId)
//                .collect(Collectors.toSet());
//    }
//
//
//}
