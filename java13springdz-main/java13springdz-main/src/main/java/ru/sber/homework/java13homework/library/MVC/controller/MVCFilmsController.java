package ru.sber.homework.java13homework.library.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.sber.homework.java13homework.library.dto.DirectorsDTO;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.dto.FilmsSearchDTO;
import ru.sber.homework.java13homework.library.dto.FilmsWithDirectorsDTO;
import ru.sber.homework.java13homework.library.exception.MyDeleteException;
import ru.sber.homework.java13homework.library.service.FilmsService;

import java.util.List;

import static ru.sber.homework.java13homework.library.constants.UserRoleConstants.ADMIN;

@Hidden
@Controller
@RequestMapping("/films")
public class MVCFilmsController {
    private final FilmsService filmsService;

    public MVCFilmsController(FilmsService filmsService) {
        this.filmsService = filmsService;
    }

    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         @ModelAttribute(name = "exception") final String exception,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        Page<FilmsWithDirectorsDTO> result;
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if (ADMIN.equalsIgnoreCase(userName)) {
            result = filmsService.getAllFilmsWithDirector(pageRequest);
        } else {
            result = filmsService.getAllNotDeletedBooksWithAuthors(pageRequest);
        }
        model.addAttribute("films", result);
        model.addAttribute("exception", exception);
        return "films/viewAllFilms";
    }

    // @MySecuredAnnotation(value = "ROLE_ADMIN")
    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("film", filmsService.getFilmsWithDirectors(id));
        return "films/viewFilm";
    }

    @GetMapping("/add")
    public String create() {
        return "films/addFilm";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmsDTO filmsDTO) {
        filmsService.create(filmsDTO);
        return "redirect:/films";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("films", filmsService.getOne(id));
        return "films/updateFilms";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("filmsForm") FilmsDTO bookDTO) {
        // @RequestParam MultipartFile file) {
//        if (file != null && file.getSize() > 0) {
//            filmsService.update(bookDTO, file);
//        }
//        else {
        filmsService.update(bookDTO);
        //}
        return "redirect:/films";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        filmsService.deleteSoft(id);
        return "redirect:/films";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        filmsService.restore(id);
        return "redirect:/films";
    }

    @PostMapping("/search")
    public String searchFilms(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("filmsSearchForm") FilmsSearchDTO filmsSearchDTO,
                              Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        model.addAttribute("films", filmsService.findFilms(filmsSearchDTO, pageRequest));
        return "films/viewAllFilms";
    }

//    @PostMapping("/search/directors")
//    public String searchBooks(@RequestParam(value = "page", defaultValue = "1") int page,
//                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
//                              @ModelAttribute("directorSearchForm") DirectorsDTO directorsDTO,
//                              Model model) {
//        FilmsSearchDTO filmsSearchDTO = new FilmsSearchDTO();
//        filmsSearchDTO.setDirectorsFIO(directorsDTO.getDirectorsFIO());
//        return searchFilms(page, pageSize, filmsSearchDTO, model);
//    }

}
