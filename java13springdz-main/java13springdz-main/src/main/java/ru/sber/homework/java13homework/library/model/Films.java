package ru.sber.homework.java13homework.library.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="films")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_Generator", sequenceName = "films_seq", allocationSize = 1)
//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class Films extends GenericModel{


    @Column(name="title", nullable = false)
    private String title;

    @Column(name="premier_year", nullable = false)
    private LocalDate premierYear;

    @Column(name="county", nullable = false)
    private String country;

    @Column(name="genre", nullable = false)
    @Enumerated
    private Genre genre;


    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            fetch = FetchType.LAZY)
    @JoinTable(name = "films_directors",
            joinColumns = @JoinColumn(name = "film_id"), foreignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name = "director_id"), inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS"))
    private Set<Directors> directors;


    @OneToMany(mappedBy = "films", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Orders> orders;

    public Films(String filmTitle1, LocalDate now, int i, int i1, String publish1, String storagePlace1, String onlineCopyPath1, String description, Genre drama, HashSet<Object> objects, HashSet<Object> objects1) {
    }
}
