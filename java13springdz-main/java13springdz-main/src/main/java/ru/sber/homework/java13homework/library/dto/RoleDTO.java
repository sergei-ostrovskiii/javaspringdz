package ru.sber.homework.java13homework.library.dto;

import lombok.*;

import java.util.Set;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RoleDTO {
    private Long id;
    private String title;
    private String description;
//    private Set<Long> usersIds;

}
