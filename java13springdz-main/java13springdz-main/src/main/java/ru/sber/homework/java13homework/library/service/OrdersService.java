package ru.sber.homework.java13homework.library.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.dto.OrdersDTO;
import ru.sber.homework.java13homework.library.mapper.OrderFilmsMapper;
import ru.sber.homework.java13homework.library.model.Orders;
import ru.sber.homework.java13homework.library.repository.OrdersRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrdersService extends GenericService<Orders, OrdersDTO> {

    private final FilmsService filmsService;
    private final OrderFilmsMapper orderFilmsMapper;
    private final OrdersRepository ordersRepository;


    protected OrdersService(OrdersRepository ordersRepository,
                                  OrderFilmsMapper orderFilmsMapper,
                                  FilmsService filmsService) {
        super(ordersRepository, orderFilmsMapper);
        this.filmsService=filmsService;
        this.orderFilmsMapper=orderFilmsMapper;
        this.ordersRepository=ordersRepository;
    }

    public Page<OrdersDTO> listUserOrderBook(final Long id,
                                             final Pageable pageable) {
        Page<Orders> objects = ordersRepository.getOrdersByUsersId(id, pageable);
        List<OrdersDTO> results = orderFilmsMapper.toDTOs(objects.getContent());
        return new PageImpl<>(results, pageable, objects.getTotalElements());
    }

    public OrdersDTO orders(OrdersDTO ordersDTO) {
        FilmsDTO filmsDTO = filmsService.getOne(ordersDTO.getFilmsId());
        //filmsDTO.setAmount(filmsDTO.getAmount() - 1);
        filmsService.update(filmsDTO);
        long rentPeriod = ordersDTO.getRentPeriod() != null ? ordersDTO.getRentPeriod() : 14L;
        ordersDTO.setRentDate(LocalDateTime.now());
      //  ordersDTO.setReturned(false);
        ordersDTO.setRentPeriod((int) rentPeriod);
      //  ordersDTO.setReturnDate(LocalDateTime.now().plusDays(rentPeriod));
        ordersDTO.setCreatedWhen(LocalDateTime.now());
     //   ordersDTO.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        return mapper.toDTO(repository.save(mapper.toEntity(ordersDTO)));
    }

//    public void returnBook(final Long id) {
//        OrdersDTO ordersDTO = getOne(id);
//        ordersDTO.setReturned(true);
//        ordersDTO.setReturnDate(LocalDateTime.now());
//        FilmsDTO filmsDTO = ordersDTO.getBookDTO();
//      //  filmsDTO.setAmount(filmsDTO.getAmount() + 1);
//        update(ordersDTO);
//        filmsService.update(filmsDTO);
//    }
}
