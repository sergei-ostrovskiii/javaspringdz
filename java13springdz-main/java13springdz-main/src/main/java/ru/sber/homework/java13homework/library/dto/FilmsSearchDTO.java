package ru.sber.homework.java13homework.library.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.sber.homework.java13homework.library.model.Genre;

@Getter
@Setter
@ToString
public class FilmsSearchDTO {
    private String title;
    private String directorsFIO;
    private Genre genre;
}
