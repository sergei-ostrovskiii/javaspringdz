package ru.sber.homework.java13homework.library.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.model.Films;
import ru.sber.homework.java13homework.library.model.GenericModel;
import ru.sber.homework.java13homework.library.repository.DirectorsRepository;
import ru.sber.homework.java13homework.library.utils.DateFormatter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmsMapper extends GenericMapper<Films, FilmsDTO> {
    // private final ModelMapper modelMapper;
    private final DirectorsRepository directorsRepository;

    protected FilmsMapper(ModelMapper modelMapper, DirectorsRepository directorsRepository) {
        super(modelMapper, Films.class, FilmsDTO.class);
//        this.modelMapper=modelMapper;
        this.directorsRepository = directorsRepository;
    }

    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(Films.class, FilmsDTO.class)
                .addMappings(m -> m.skip(FilmsDTO::setDirectorsId)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmsDTO.class, Films.class)
                .addMappings(m -> m.skip(Films::setDirectors)).setPostConverter(toEntityConverter())
                .addMappings(m->m.skip(Films::setPremierYear)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmsDTO source, Films destination) {
        if (!Objects.isNull(source.getDirectorsId())) {
            destination.setDirectors(new HashSet<>(directorsRepository.findAllById(source.getDirectorsId())));
        } else {
            destination.setDirectors(Collections.emptySet());
        }
        if(source.getPremierYear()!=null) {destination.setPremierYear(DateFormatter.formatStringToDate(source.getPremierYear()));}

    }

    @Override
    protected void mapSpecificFields(Films source, FilmsDTO destination) {
        destination.setDirectorsId(getIds(source));
    }

    protected Set<Long> getIds(Films films) {
        return Objects.isNull(films) || Objects.isNull(films.getDirectors())
                ? Collections.emptySet()
                : films.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
