package ru.sber.homework.java13homework.library.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.homework.java13homework.library.model.Directors;
import ru.sber.homework.java13homework.library.model.Films;
import ru.sber.homework.java13homework.library.model.Genre;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class FilmsDTO extends GenericDTO {
    private String title;
    private String premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorsId;
    private Set<Long> ordersIds;
    private boolean isDeleted;

    public FilmsDTO(Films films) {
        FilmsDTO filmsDTO = new FilmsDTO();
        //из entity делаем DTO
        filmsDTO.setTitle(films.getTitle());
        filmsDTO.setGenre(films.getGenre());
//        filmsDTO.se(book.getDescription());
        filmsDTO.setId(films.getId());
        //      filmsDTO.setPageCount(book.getPageCount());
//        filmsDTO.setPublishDate(book.getPublishDate().toString());
        Set<Directors> directors = films.getDirectors();
        Set<Long> directorsIds = new HashSet<>();
        if (directors != null && directors.size() > 0) {
            directors.forEach(a -> directorsIds.add(a.getId()));
        }
        filmsDTO.setDirectorsId(directorsIds);
    }

    public <E> FilmsDTO(String title1, String premierYear1, String country1, int i, Genre drama, HashSet<E> es, HashSet<E> es1, boolean b) {
    }

    public <E> FilmsDTO(String filmTitle2, String publishDate2, int i, int i1, String storagePlace2, String onlineCopyPath2, String publish2, String description2, Genre novel, HashSet<E> es, boolean b) {
    }
}

