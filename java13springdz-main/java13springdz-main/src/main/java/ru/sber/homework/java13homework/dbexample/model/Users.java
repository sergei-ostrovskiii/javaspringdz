package ru.sber.homework.java13homework.dbexample.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@ToString
public class Users {
    private Long userId;
    private String lastName;
    private String firstName;
    private Date birthday;
    private String phone;
    private String email;
    private String rentedBooks;

    public Users(String lastName, String firstName, String phone, String email, String rentedBooks) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.phone = phone;
        this.email = email;
        this.rentedBooks = rentedBooks;

    }

}
