package ru.sber.homework.java13homework.library.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;
import ru.sber.homework.java13homework.library.constants.Errors;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.dto.FilmsSearchDTO;
import ru.sber.homework.java13homework.library.dto.FilmsWithDirectorsDTO;
import ru.sber.homework.java13homework.library.exception.MyDeleteException;
import ru.sber.homework.java13homework.library.mapper.FilmsMapper;
import ru.sber.homework.java13homework.library.mapper.FilmsWithDirectorsMapper;
import ru.sber.homework.java13homework.library.model.Films;
import ru.sber.homework.java13homework.library.repository.FilmsRepository;

import java.util.List;

@Service
public class FilmsService extends GenericService<Films, FilmsDTO> {

//    private final FilmsWithDirectorsMapper filmsWithDirectorsMapper;
//    private final FilmsRepository filmsRepository;
//    public FilmsService(FilmsRepository filmsRepository,
//                        FilmsMapper filmMapper,
//                        FilmsWithDirectorsMapper filmsWithDirectorsMapper ) {
//
//        super(filmsRepository, filmMapper);
//        this.filmsWithDirectorsMapper=filmsWithDirectorsMapper;
//        this.filmsRepository=filmsRepository;
//
//
//    }
//
//    public List<FilmsWithDirectorsDTO> getAllFilmsWithDirectors() {
//        return filmsWithDirectorsMapper.toDTOs(filmsRepository.findAll());
//    }

    private final FilmsRepository repository;
    private final FilmsWithDirectorsMapper filmsWithDirectorsMapper;

    protected FilmsService(FilmsRepository repository,
                          FilmsMapper mapper,
                           FilmsWithDirectorsMapper filmsWithDirectorsMapper) {
        //Передаем этот репозиторй в абстрактный севрис,
        //чтобы он понимал с какой таблицей будут выполняться CRUD операции
        super(repository, mapper);
        this.repository = repository;
        this.filmsWithDirectorsMapper=filmsWithDirectorsMapper;
    }

    public Page<FilmsWithDirectorsDTO> getAllFilmsWithDirector(Pageable pageable) {
        Page<Films> filmsPaginated = repository.findAll(pageable);
        List<FilmsWithDirectorsDTO> result = filmsWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    public Page<FilmsWithDirectorsDTO> getAllNotDeletedBooksWithAuthors(Pageable pageable) {
        Page<Films> booksPaginated = repository.findAllByIsDeletedFalse(pageable);
        List<FilmsWithDirectorsDTO> result = filmsWithDirectorsMapper.toDTOs(booksPaginated.getContent());
        return new PageImpl<>(result, pageable, booksPaginated.getTotalElements());
    }

    public FilmsWithDirectorsDTO getFilmsWithDirectors(Long id) {
        return filmsWithDirectorsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    public Page<FilmsWithDirectorsDTO> findFilms(FilmsSearchDTO filmsSearchDTO,
                                                 Pageable pageable) {
        String genre = filmsSearchDTO.getGenre() != null ? String.valueOf(filmsSearchDTO.getGenre().ordinal()) : null;
        Page<Films> booksPaginated = repository.searchFilms(genre,
                filmsSearchDTO.getTitle(),
                filmsSearchDTO.getDirectorsFIO(),
                pageable
        );
        List<FilmsWithDirectorsDTO> result = filmsWithDirectorsMapper.toDTOs(booksPaginated.getContent());
        return new PageImpl<>(result, pageable, booksPaginated.getTotalElements());
    }

    // files/books/year/month/day/file_name_{id}_{created_when}.txt
    // files/книга_id.pdf
//    public FilmsDTO create(final FilmsDTO object,
//                          MultipartFile file) {
//        String fileName = FileHelper.createFile(file);
//        object.setOnlineCopyPath(fileName);
//        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
//        object.setCreatedWhen(LocalDateTime.now());
//        return mapper.toDTO(repository.save(mapper.toEntity(object)));
//    }

//    public BookDTO update(final BookDTO object,
//                          MultipartFile file) {
//        String fileName = FileHelper.createFile(file);
//        object.setOnlineCopyPath(fileName);
//        return mapper.toDTO(repository.save(mapper.toEntity(object)));
//    }

    @Override
    public void deleteSoft(Long id) throws MyDeleteException {
        Films films = repository.findById(id).orElseThrow(
                () -> new NotFoundException("Книги с заданным ID=" + id + " не существует"));
        //boolean filmsCanBeDeleted = repository.findFilmsByIdAndFilmsRentInfosReturnedFalseAndIsDeletedFalse(id) == null;
        boolean filmsCanBeDeleted = repository.checkFilmForDeletion(id);
        if (filmsCanBeDeleted) {
//            if (films.getOnlineCopyPath() != null && !book.getOnlineCopyPath().isEmpty()) {
//                FileHelper.deleteFile(book.getOnlineCopyPath());
//            }
            markAsDeleted(films);
            repository.save(films);
        }
        else {
            throw new MyDeleteException(Errors.Books.BOOK_DELETE_ERROR);
        }
    }

    public void restore(Long objectId) {
        Films films = repository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Книги с заданным ID=" + objectId + " не существует"));
        unMarkAsDeleted(films);
        repository.save(films);
    }



}
