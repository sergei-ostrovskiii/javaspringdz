package ru.sber.homework.java13homework.library.dto;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DirectorsDTO extends GenericDTO {
    private String directorsFIO;
    private String position;
    private Set<Long> filmsId;
    private boolean isDeleted;

    public <E> DirectorsDTO(String mvc_testAuthorFio, String s, String test_description, HashSet<E> es, boolean b) {
    }
}
