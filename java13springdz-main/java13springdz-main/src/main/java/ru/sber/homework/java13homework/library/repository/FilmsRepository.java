package ru.sber.homework.java13homework.library.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sber.homework.java13homework.library.model.Films;

@Repository
public interface FilmsRepository extends GenericRepository<Films> {
    @Query(nativeQuery = true,
            value = """
                 select distinct b.*
                 from films b
                 left join films_directors ba on b.id = ba.film_id
                 join directors a on a.id = ba.director_id
                 where b.title ilike '%' || btrim(coalesce(:title, b.title)) || '%'                
                 and cast(b.genre as char) like coalesce(:genre,'%')
                 and a.directors_fio ilike '%' || :fio || '%'
                 and b.is_deleted = false
                      """)
    Page<Films> searchFilms(@Param(value = "genre") String genre,
                            @Param(value = "title") String title,
                            @Param(value = "fio") String fio,
                            Pageable pageable);

   // Films findFilmsByIdAndFilmsRentInfosReturnedFalseAndIsDeletedFalse(final Long i);
  //  Films findFilmsBy(final long id);

    @Query("""
          select case when count(b) > 0 then false else true end
          from Films b join Orders bri on b.id = bri.films.id
          where b.id = :id
          """)
    boolean checkFilmForDeletion(final Long id);
    Page<Films> findAllByIsDeletedFalse(Pageable pageable);
}

