package ru.sber.homework.java13homework.library.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.homework.java13homework.library.dto.DirectorsDTO;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.model.Directors;
import ru.sber.homework.java13homework.library.service.DirectorsService;
import ru.sber.homework.java13homework.library.service.FilmsService;

@RestController
@RequestMapping(value = "/directors")
@Tag(name = "Режиссеры",
        description = "Контроллер для работы с режисеррами")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DirectorsController extends GenericController<Directors, DirectorsDTO> {

    private final DirectorsService directorsService;
    private final FilmsService filmsService;

    public DirectorsController(DirectorsService directorsService, FilmsService filmService) {
        super(directorsService);
        this.directorsService = directorsService;
        this.filmsService = filmService;
    }

    @Operation(description = "Добавить фильм к директору", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorsDTO> addFilm(@RequestParam(value = "filmId") Long filmId,
                                                @RequestParam(value = "directorId") Long directorId) {
        DirectorsDTO directorDTO = directorsService.getOne(directorId);
        FilmsDTO filmDTO = filmsService.getOne(filmId);
        directorDTO.getFilmsId().add(filmDTO.getId());
        return ResponseEntity.status(HttpStatus.OK).body(directorsService.update(directorDTO));
    }

}
