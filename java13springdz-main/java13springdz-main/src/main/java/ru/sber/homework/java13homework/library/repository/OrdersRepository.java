package ru.sber.homework.java13homework.library.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import ru.sber.homework.java13homework.library.model.Orders;

@Repository
public interface OrdersRepository extends GenericRepository<Orders>{
    Page<Orders> getOrdersByUsersId(Long userId,
                                   Pageable pageable);
}
