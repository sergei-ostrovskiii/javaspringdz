package ru.sber.homework.java13homework.library.REST.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.homework.java13homework.library.dto.OrdersDTO;
import ru.sber.homework.java13homework.library.model.Orders;
import ru.sber.homework.java13homework.library.service.OrdersService;

@RestController
@RequestMapping("/rest/rent/info")
@Tag(name = "Аренда книг",
        description = "Контроллер для работы с арендой/выдачей книг пользователям библиотеки")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class OrderFilmController extends GenericController<Orders, OrdersDTO> {
    private OrderFilmController(OrdersService ordersService){
        super(ordersService);
    }
}
