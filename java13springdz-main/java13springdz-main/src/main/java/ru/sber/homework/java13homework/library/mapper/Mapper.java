package ru.sber.homework.java13homework.library.mapper;

import ru.sber.homework.java13homework.library.dto.GenericDTO;
import ru.sber.homework.java13homework.library.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);
    D toDTO(E entity);
    List<E> toEntities(List<D> dtos);
    List<D> toDTOs(List<E> entities);
}
