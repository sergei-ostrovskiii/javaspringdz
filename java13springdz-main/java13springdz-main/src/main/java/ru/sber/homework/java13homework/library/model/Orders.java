package ru.sber.homework.java13homework.library.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name="orders")
@SequenceGenerator(name="default_gen", sequenceName = "orders_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class Orders extends GenericModel {
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "users_id",foreignKey = @ForeignKey(name="FC_ORDERS_USERS"))
    private Users users;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "Films_id",foreignKey = @ForeignKey(name="FC_ORDERS_FILMS"))
    private Films films;

    @Column(name="rent_date",nullable = false)
    private LocalDateTime rentDate;

    @Column(name="rent_period", nullable = false)
    private Integer rentPeriod;

    @Column(name="purchase" , nullable = false)
    private boolean purchase;

    public Orders(Object o, Object o1, LocalDateTime now, LocalDateTime now1, boolean b, int i) {
    }
}
