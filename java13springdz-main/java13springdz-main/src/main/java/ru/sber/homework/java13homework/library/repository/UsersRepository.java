package ru.sber.homework.java13homework.library.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.sber.homework.java13homework.library.model.Users;

import java.util.List;

@Repository
public interface UsersRepository extends GenericRepository<Users> {

    Users findUserByLogin(String login);

    Users findUserByLoginAndIsDeletedFalse(String login);

    Users findUserByEmail(String email);

    Users findUserByChangePasswordToken(String token);

    @Query(nativeQuery = true,
            value = """
                    select u.*
                    from users u
                    where u.first_name ilike '%' || coalesce(:firstName, '%') || '%'
                    and u.last_name ilike '%' || coalesce(:lastName, '%') || '%'
                    and u.login ilike '%' || coalesce(:login, '%') || '%'
                     """)
    Page<Users> searchUsers(String firstName,
                            String lastName,
                            String login,
                            Pageable pageable);

    @Query(nativeQuery = true,
            value = """
                    select email
                    from users u join orders bri on u.id = bri.users_id
                    where bri.rent_date + interval rent_period >= now()
                    and u.is_deleted = false
                    """)
    List<String> getDelayedEmails();

    //and bri.returned = false

}
