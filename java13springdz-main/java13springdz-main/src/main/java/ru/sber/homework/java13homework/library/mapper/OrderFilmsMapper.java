package ru.sber.homework.java13homework.library.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import ru.sber.homework.java13homework.library.dto.OrdersDTO;
import ru.sber.homework.java13homework.library.model.Orders;
import ru.sber.homework.java13homework.library.repository.FilmsRepository;
import ru.sber.homework.java13homework.library.repository.UsersRepository;
import ru.sber.homework.java13homework.library.service.FilmsService;

import java.util.Set;

@Component
public class OrderFilmsMapper extends GenericMapper<Orders, OrdersDTO> {

    private final UsersRepository usersRepository;
    private final FilmsRepository filmsRepository;
    private final FilmsService filmsService;

    protected OrderFilmsMapper(ModelMapper modelMapper,
                               UsersRepository usersRepository,
                               FilmsRepository filmsRepository,
                                FilmsService filmsService) {
        super(modelMapper, Orders.class, OrdersDTO.class);
        this.usersRepository = usersRepository;
        this.filmsRepository = filmsRepository;
        this.filmsService=filmsService;

    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Orders.class, OrdersDTO.class)
                .addMappings(m -> m.skip(OrdersDTO::setUsersId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(OrdersDTO::setFilmsId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(OrdersDTO::setFilmsDTO)).setPostConverter(toDTOConverter());
        super.modelMapper.createTypeMap(OrdersDTO.class, Orders.class)
                .addMappings(m -> m.skip(Orders::setUsers)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Orders::setFilms)).setPostConverter(toEntityConverter());
    }


    @Override
    protected void mapSpecificFields(OrdersDTO source, Orders destination) {
        destination.setFilms(filmsRepository.findById(source.getFilmsId()).orElseThrow(() -> new NotFoundException("Фильм не найден")));
        destination.setUsers(usersRepository.findById(source.getUsersId()).orElseThrow(() -> new NotFoundException("Пользователь не найден")));
    }

    @Override
    protected void mapSpecificFields(Orders source, OrdersDTO destination) {
        destination.setUsersId(source.getUsers().getId());
        destination.setFilmsId(source.getFilms().getId());
        destination.setFilmsDTO(filmsService.getOne(source.getFilms().getId()));
    }

    @Override
    protected Set<Long> getIds(Orders entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}
