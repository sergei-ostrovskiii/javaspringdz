package ru.sber.homework.java13homework.library.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(name = "uniqueEmail", columnNames = "email"),
        @UniqueConstraint(name = "uniqueLogin", columnNames = "login")}
)
@SequenceGenerator(name = "default_Generator", sequenceName = "users_seq", allocationSize = 1)
public class Users extends GenericModel {

    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "birth_date", nullable = false)
    private LocalDate birthDate;

    @Column(name = "phone", nullable = false)
    private Integer phone;

    @Column(name = "address")
    private String address;

    @Column(name = "email", nullable = false)
    private String email;
//
@Column(name = "change_password_token")
private String changePasswordToken;

//    @Column(name = "created_when", nullable = false)
//    private LocalDate createdWhen;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "role_id", nullable = false, foreignKey = @ForeignKey(name = "FC_USER_ROLES"))
    private Role role;

    @OneToMany(mappedBy = "users", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Orders> orders;


    public Users(String login, String password, String email, LocalDate now, String firstName, String lastName, String middleName, String phone, String address, String changePasswordToken, Role role, HashSet<Object> objects) {
    }
}
