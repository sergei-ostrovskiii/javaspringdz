package ru.sber.homework.java13homework.library.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ru.sber.homework.java13homework.library.dto.AddFilmsDTO;
import ru.sber.homework.java13homework.library.dto.DirectorsDTO;
import ru.sber.homework.java13homework.library.exception.MyDeleteException;
import ru.sber.homework.java13homework.library.service.DirectorsService;
import ru.sber.homework.java13homework.library.service.FilmsService;

import static ru.sber.homework.java13homework.library.constants.UserRoleConstants.ADMIN;

@Controller
@Hidden
@Slf4j
@RequestMapping("/directors")
public class MVCDirectorsController {
    private final DirectorsService directorsService;
    private final FilmsService filmsService;


    public MVCDirectorsController(DirectorsService directorsService,
                               FilmsService filmsService) {
        this.directorsService=directorsService;
        this.filmsService=filmsService;
    }

    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         @ModelAttribute(name = "exception") final String exception,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "directorsFIO"));
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        Page<DirectorsDTO> result;
        if (ADMIN.equalsIgnoreCase(userName)) {
            result = directorsService.listAll(pageRequest);
        }
        else {
           result = directorsService.listAllNotDeleted(pageRequest);
        }
        model.addAttribute("directors", result);
        model.addAttribute("exception", exception);
        return "directors/viewAllDirector";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("directors", directorsService.getOne(id));
        return "directors/viewDirector";
    }

    @GetMapping("/add")
    public String create() {
        return "directors/addDirector";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("directorsForm") DirectorsDTO directorsDTO) {
        directorsService.create(directorsDTO);
        return "redirect:/directors";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("directors", directorsService.getOne(id));
        return "directors/updateDirector";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("directorsForm") DirectorsDTO directorsDTO) {

        directorsService.update(directorsDTO);
        return "redirect:/directors";
    }

    @GetMapping("/add-films/{directorId}")
    public String addFilms(@PathVariable Long directorId,
                          Model model) {
        model.addAttribute("films", filmsService.listAll());
        model.addAttribute("directorsId", directorId);
        model.addAttribute("directors", directorsService.getOne(directorId).getDirectorsFIO());
        return "directors/addDirectorsFilms";
    }

    @PostMapping("/add-book")
    public String addFilms(@ModelAttribute("directorsFilmsForm") AddFilmsDTO addFilmsDTO) {
        directorsService.addFilms(addFilmsDTO);
        return "redirect:/directors";
    }

    @PostMapping("/search")
    public String searchBooks(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("directorsSearchForm") DirectorsDTO directorsDTO,
                              Model model) {
        if (StringUtils.hasText(directorsDTO.getDirectorsFIO()) || StringUtils.hasLength(directorsDTO.getDirectorsFIO())) {
            PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "directorsFIO"));
            model.addAttribute("directors", directorsService.searchDirectors(directorsDTO.getDirectorsFIO().trim(), pageRequest));
            return "directors/viewAllDirector";
        }
        else {
            return "redirect:/directors";
        }
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        directorsService.deleteSoft(id);
        return "redirect:/directors";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        directorsService.restore(id);
        return "redirect:/directors";
    }

    @ExceptionHandler(MyDeleteException.class)
    public RedirectView handleError(HttpServletRequest req,
                                    Exception ex,
                                    RedirectAttributes redirectAttributes) {
        log.error("Запрос: " + req.getRequestURL() + " вызвал ошибку " + ex.getMessage());
        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/directors", true);
    }

}
