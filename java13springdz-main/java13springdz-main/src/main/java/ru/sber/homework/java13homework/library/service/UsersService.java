package ru.sber.homework.java13homework.library.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.sber.homework.java13homework.library.constants.MailConstants;
import ru.sber.homework.java13homework.library.dto.RoleDTO;
import ru.sber.homework.java13homework.library.dto.UsersDTO;
import ru.sber.homework.java13homework.library.mapper.UsersMapper;
import ru.sber.homework.java13homework.library.model.Users;
import ru.sber.homework.java13homework.library.repository.UsersRepository;
import ru.sber.homework.java13homework.library.utils.MailUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class UsersService extends GenericService<Users, UsersDTO> {


    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JavaMailSender javaMailSender;

    protected UsersService(UsersRepository usersRepository,
                           UsersMapper usersMapper,
                           BCryptPasswordEncoder bCryptPasswordEncoder,
                           JavaMailSender javaMailSender ) {
        super(usersRepository, usersMapper);
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.javaMailSender=javaMailSender;
    }

    public UsersDTO getUserByLogin(final String login) {
        return mapper.toDTO(((UsersRepository) repository).findUserByLogin(login));
    }

    public UsersDTO getUserByEmail(final String email) {
        return mapper.toDTO(((UsersRepository) repository).findUserByEmail(email));
    }

    public Boolean checkPassword(String password, UserDetails userDetails) {
        return bCryptPasswordEncoder.matches(password, userDetails.getPassword());
    }

    @Override
    public UsersDTO create(UsersDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        object.setRole(roleDTO);
        //object.setCreatedBy("REGISTRATION FORM");
        object.setCreatedWhen(LocalDateTime.now());
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }


    public void sendChangePasswordEmail(final UsersDTO usersDTO) {
        UUID uuid = UUID.randomUUID();
        usersDTO.setChangePasswordToken(uuid.toString());
        update(usersDTO);
        SimpleMailMessage mailMessage = MailUtils.createEmailMessage(usersDTO.getEmail(),
                MailConstants.MAIL_SUBJECT_FOR_REMEMBER_PASSWORD,
                MailConstants.MAIL_MESSAGE_FOR_REMEMBER_PASSWORD + uuid);
        javaMailSender.send(mailMessage);
    }

    public void changePassword(final String uuid,
                               final String password) {
        UsersDTO user = mapper.toDTO(((UsersRepository) repository).findUserByChangePasswordToken(uuid));
        user.setChangePasswordToken(null);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        update(user);
    }

    public List<String> getUserEmailsWithDelayedRentDate() {
        return ((UsersRepository) repository).getDelayedEmails();
    }

    public Page<UsersDTO> findUsers(UsersDTO userDTO,
                                   Pageable pageable) {
        Page<Users> users = ((UsersRepository) repository).searchUsers(userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getLogin(),
                pageable);
        List<UsersDTO> result = mapper.toDTOs(users.getContent());
        return new PageImpl<>(result, pageable, users.getTotalElements());
    }

}
