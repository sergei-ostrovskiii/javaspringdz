package ru.sber.homework.java13homework.library.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddFilmsDTO {
    Long FilmsId;
    Long DirectorsId;
}
