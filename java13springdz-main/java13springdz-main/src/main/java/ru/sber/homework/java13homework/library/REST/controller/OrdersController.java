package ru.sber.homework.java13homework.library.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.dto.OrdersDTO;
import ru.sber.homework.java13homework.library.dto.UsersDTO;
import ru.sber.homework.java13homework.library.model.Orders;
import ru.sber.homework.java13homework.library.service.FilmsService;
import ru.sber.homework.java13homework.library.service.OrdersService;
import ru.sber.homework.java13homework.library.service.UsersService;

@RestController
@RequestMapping("/orders")
@Tag(name = "Заказы", description = "Контроллер для работы с заказами")
public class OrdersController extends GenericController<Orders, OrdersDTO> {
    private final OrdersService ordersService;
    private final FilmsService filmsService;
    private final UsersService usersService;

    public OrdersController(OrdersService ordersService, FilmsService filmsService, UsersService usersService) {
        super(ordersService);
        this.ordersService = ordersService;
        this.filmsService = filmsService;
        this.usersService = usersService;
    }

    @Operation(description = "Взять фильм в аренду", method = "create")
    @RequestMapping(value = "/addOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrdersDTO> create(@RequestBody OrdersDTO newEntity) {
        FilmsDTO filmsDTO = filmsService.getOne(newEntity.getFilmsId());
        UsersDTO usersDTO = usersService.getOne(newEntity.getUsersId());
        filmsDTO.getOrdersIds().add(newEntity.getId());
        usersDTO.getOrdersIds().add(newEntity.getId());


        return ResponseEntity.status(HttpStatus.CREATED).body(ordersService.create(newEntity));
    }

}
