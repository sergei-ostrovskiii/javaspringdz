package ru.sber.homework.java13homework.library.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.homework.java13homework.library.model.Role;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class UsersDTO extends GenericDTO {
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private String birthDate;
    private Integer phone;
    private String address;
    private String email;
    //    private LocalDate createdWhen;
    private String changePasswordToken;
    private RoleDTO role;
    private Set<Long> ordersIds;

    public UsersDTO(String login, String password, String email, String birthDate, String firstName, String lastName, String middleName, String phone, String address, RoleDTO roleDTO, String changePasswordToken, HashSet<Object> objects, boolean b) {
    }
}
