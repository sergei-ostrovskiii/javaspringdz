package ru.sber.homework.java13homework.dbexample.constants;

public interface DBConsts {
    String DB_HOST = "localhost";
    String DB = "local_db";
    String USER = "postgres";
    String PASSWORD = "12345";
    String PORT = "5432";
}
