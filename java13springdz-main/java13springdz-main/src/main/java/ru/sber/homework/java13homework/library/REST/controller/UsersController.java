package ru.sber.homework.java13homework.library.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.sber.homework.java13homework.library.config.jwt.JWTTokenUtil;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.dto.LoginDTO;
import ru.sber.homework.java13homework.library.dto.OrdersDTO;
import ru.sber.homework.java13homework.library.dto.UsersDTO;
import ru.sber.homework.java13homework.library.model.Users;
import ru.sber.homework.java13homework.library.service.FilmsService;
import ru.sber.homework.java13homework.library.service.OrdersService;
import ru.sber.homework.java13homework.library.service.UsersService;
import ru.sber.homework.java13homework.library.service.userdetails.CustomUserDetailsService;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequestMapping("/rest/users")
@Tag(name = "Пользователи", description = "Контроллер по управлению пользователями")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UsersController
        extends GenericController<Users, UsersDTO> {
    private final UsersService usersService;
    private final OrdersService ordersService;
    private final FilmsService filmsService;
    private final CustomUserDetailsService customUserDetailsService;
    private final JWTTokenUtil jwtTokenUtil;

    public UsersController(UsersService usersService,
                           OrdersService orderService,
                           CustomUserDetailsService customUserDetailsService,
                           JWTTokenUtil jwtTokenUtil,
                           FilmsService filmService) {
        super(usersService);
        this.usersService = usersService;
        this.ordersService = orderService;
        this.filmsService = filmService;
        this.jwtTokenUtil=jwtTokenUtil;
        this.customUserDetailsService = customUserDetailsService;
    }

    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
        Map<String, Object> response = new HashMap<>();
        log.info("LoginDTO: {}", loginDTO);
        UserDetails foundUser =  customUserDetailsService.loadUserByUsername(loginDTO.getLogin());
        log.info("foundUser, {}", foundUser);
        if (!usersService.checkPassword(loginDTO.getPassword(), foundUser)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации!\nНеверный пароль");
        }
        String token = jwtTokenUtil.generateToken(foundUser);
        response.put("token", token);
        response.put("username", foundUser.getUsername());
        response.put("authorities", foundUser.getAuthorities());
        log.debug("TOKEN {}"+ token);
        log.debug("USERNAME {}" +jwtTokenUtil.getUsernameFromToken(token));
        return ResponseEntity.ok().body(response);
    }



    @Operation(description = "Получить список всех фильмов пользователя", method = "getAllFilms")
    @RequestMapping(value = "/getAllFilms", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<FilmsDTO>> addFilm(@RequestParam(value = "id") Long id) {
        UsersDTO usersDTO = usersService.getOne(id);
        Set<Long> ordersIds = usersDTO.getOrdersIds();
        Set<FilmsDTO> filmDTOs = ordersIds.stream()
                .map(ordersService::getOne)
                .map(OrdersDTO::getFilmsId)
                .map(filmsService::getOne)
                .collect(Collectors.toSet());

        return ResponseEntity.status(HttpStatus.OK).body(filmDTOs);
    }


}
