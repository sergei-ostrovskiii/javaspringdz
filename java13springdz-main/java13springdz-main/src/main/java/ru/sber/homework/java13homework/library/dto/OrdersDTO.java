package ru.sber.homework.java13homework.library.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrdersDTO extends GenericDTO{

    private LocalDateTime rentDate;
    private Integer rentPeriod;
    private Long usersId;
    private Long filmsId;
    private FilmsDTO filmsDTO;

    public OrdersDTO(LocalDateTime now, LocalDateTime now1, boolean b, int i, long l, long l1, Object o) {
    }
}
