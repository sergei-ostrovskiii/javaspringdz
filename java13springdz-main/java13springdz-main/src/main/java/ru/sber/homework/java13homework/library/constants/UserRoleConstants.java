package ru.sber.homework.java13homework.library.constants;

public interface UserRoleConstants {
    String ADMIN="ADMIN";
    String LIBRARIAN="LIBRARIAN";
    String USER="USER";
}
