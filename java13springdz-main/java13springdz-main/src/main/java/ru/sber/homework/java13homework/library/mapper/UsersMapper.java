package ru.sber.homework.java13homework.library.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.homework.java13homework.library.dto.UsersDTO;
import ru.sber.homework.java13homework.library.model.GenericModel;
import ru.sber.homework.java13homework.library.model.Users;
import ru.sber.homework.java13homework.library.repository.OrdersRepository;
import ru.sber.homework.java13homework.library.utils.DateFormatter;


import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UsersMapper extends GenericMapper<Users, UsersDTO> {


    private OrdersRepository ordersRepository;

    protected UsersMapper(ModelMapper modelMapper,
                         OrdersRepository orderRepository) {
        super(modelMapper, Users.class, UsersDTO.class);
        this.ordersRepository = orderRepository;
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Users.class, UsersDTO.class)
                .addMappings(m -> m.skip(UsersDTO::setOrdersIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UsersDTO.class, Users.class)
                .addMappings(m -> m.skip(Users::setOrders)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Users::setBirthDate)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UsersDTO source, Users destination) {
        if (!Objects.isNull(source.getOrdersIds())) {
            destination.setOrders(new HashSet<>(ordersRepository.findAllById(source.getOrdersIds())));
        } else {
            destination.setOrders(Collections.emptySet());
        }
        destination.setBirthDate(DateFormatter.formatStringToDate(source.getBirthDate()));
    }

    @Override
    protected void mapSpecificFields(Users source, UsersDTO destination) {
        destination.setOrdersIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Users entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getOrders())
                ? null
                : entity.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }


}
