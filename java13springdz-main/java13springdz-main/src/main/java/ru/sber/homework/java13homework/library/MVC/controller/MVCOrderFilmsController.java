package ru.sber.homework.java13homework.library.MVC.controller;

import groovy.util.logging.Slf4j;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sber.homework.java13homework.library.dto.OrdersDTO;
import ru.sber.homework.java13homework.library.service.FilmsService;
import ru.sber.homework.java13homework.library.service.OrdersService;
import ru.sber.homework.java13homework.library.service.userdetails.CustomUserDetails;

@Controller
@Slf4j
@Hidden
@RequestMapping("/orders")
public class MVCOrderFilmsController {
    private final OrdersService ordersService;
    private final FilmsService filmsService;

    public MVCOrderFilmsController(OrdersService ordersService, FilmsService filmsService) {
        this.ordersService = ordersService;
        this.filmsService=filmsService;
    }

    @GetMapping("/films/{filmsId}")
    public String orders(@PathVariable Long filmsId,
                           Model model) {
        model.addAttribute("films", filmsService.getOne(filmsId));
        return "userFilms/orderFilms";
    }

    @PostMapping("/films")
    public String orders(@ModelAttribute("orderFilmsForm")OrdersDTO ordersDTO) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ordersDTO.setUsersId(Long.valueOf(customUserDetails.getUserId()));
        ordersService.orders(ordersDTO);
        return "redirect:/orders/user-films/" + customUserDetails.getUserId();
    }

    @GetMapping("/user-films/{id}")
    public String userBooks(@RequestParam(value = "page", defaultValue = "1") int page,
                            @RequestParam(value = "size", defaultValue = "5") int pageSize,
                            @PathVariable Long id,
                            Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
        Page<OrdersDTO> rentInfoDTOPage = ordersService.listUserOrderBook(id, pageRequest);
        model.addAttribute("orders", rentInfoDTOPage);
        return "userFilms/viewAllUserFilms";
    }

    //
//    @GetMapping("/return-book/{id}")
//    public String returnBook(@PathVariable Long id) {
//        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        ordersService.returnBook(id);
//        return "redirect:/rent/user-books/" + customUserDetails.getUserId();
//    }
}
