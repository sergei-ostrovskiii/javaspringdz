package ru.sber.homework.java13homework.library.repository;

import org.springframework.stereotype.Repository;
import ru.sber.homework.java13homework.library.model.Role;

@Repository
public interface RoleRepository extends GenericRepository<Role> {
}
