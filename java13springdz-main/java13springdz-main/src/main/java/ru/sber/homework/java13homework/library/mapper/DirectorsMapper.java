package ru.sber.homework.java13homework.library.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.homework.java13homework.library.dto.DirectorsDTO;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.model.Directors;
import ru.sber.homework.java13homework.library.model.Films;
import ru.sber.homework.java13homework.library.model.GenericModel;
import ru.sber.homework.java13homework.library.repository.FilmsRepository;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DirectorsMapper extends GenericMapper<Directors, DirectorsDTO> {

    private final FilmsRepository filmsRepository;
    private final ModelMapper modelMapper;

    protected DirectorsMapper(ModelMapper modelMapper, FilmsRepository filmsRepository) {
        super(modelMapper, Directors.class, DirectorsDTO.class);
        this.filmsRepository = filmsRepository;
        this.modelMapper = modelMapper;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Directors.class, DirectorsDTO.class)
                .addMappings(m -> m.skip(DirectorsDTO::setFilmsId)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(DirectorsDTO.class, Directors.class)
                .addMappings(m -> m.skip(Directors::setFilms)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DirectorsDTO source, Directors destination) {
        if (!Objects.isNull(source.getFilmsId())) {
            destination.setFilms(new HashSet<>(filmsRepository.findAllById(source.getFilmsId())));
        } else {
            destination.setFilms(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Directors source, DirectorsDTO destination) {
        destination.setFilmsId(Objects.isNull(source) || Objects.isNull(source.getFilms()) ? null
                : source.getFilms().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet()));

    }

    protected Set<Long> getIds(Directors directors) {
        return Objects.isNull(directors) || Objects.isNull(directors.getFilms())
                ? null
                : directors.getFilms().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }


}
