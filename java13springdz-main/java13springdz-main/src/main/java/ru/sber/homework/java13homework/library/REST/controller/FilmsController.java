package ru.sber.homework.java13homework.library.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.homework.java13homework.library.dto.DirectorsDTO;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.model.Films;
import ru.sber.homework.java13homework.library.service.DirectorsService;
import ru.sber.homework.java13homework.library.service.FilmsService;

@RestController
@RequestMapping(value = "/films")
@Tag(name = "Фильмы",
        description = "Контроллер для работы с фильмотекой")
public class FilmsController extends GenericController<Films, FilmsDTO> {

    private final FilmsService filmService;
    private final DirectorsService directorService;

    public FilmsController(FilmsService filmsService, DirectorsService directorsService) {
        super(filmsService);
        this.filmService = filmsService;
        this.directorService = directorsService;
    }

    @Operation(description = "Добавить директора к фильму", method = "addDirector")
    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmsDTO> addDirector(@RequestParam(value = "filmId") Long filmId,
                                                @RequestParam(value = "directorId") Long directorId) {
        FilmsDTO filmDTO = filmService.getOne(filmId);
        DirectorsDTO directorsDTO = directorService.getOne(directorId);
        filmDTO.getDirectorsId().add(directorsDTO.getId());
        return ResponseEntity.status(HttpStatus.OK).body(filmService.update(filmDTO));
    }



}
