package ru.sber.homework.java13homework.library.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.homework.java13homework.library.dto.FilmsDTO;
import ru.sber.homework.java13homework.library.dto.FilmsWithDirectorsDTO;
import ru.sber.homework.java13homework.library.model.Films;
import ru.sber.homework.java13homework.library.model.GenericModel;
import ru.sber.homework.java13homework.library.repository.DirectorsRepository;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmsWithDirectorsMapper extends GenericMapper<Films, FilmsWithDirectorsDTO> {

    private final DirectorsRepository directorsRepository;

    protected FilmsWithDirectorsMapper(ModelMapper modelMapper,  DirectorsRepository directorsRepository) {
        super(modelMapper, Films.class, FilmsWithDirectorsDTO.class);
        this.directorsRepository=directorsRepository;
    }

    @PostConstruct
    protected  void setupMapper(){
        modelMapper.createTypeMap(Films.class, FilmsWithDirectorsDTO.class)
                .addMappings(m->m.skip(FilmsWithDirectorsDTO::setDirectorsId)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmsWithDirectorsDTO.class, Films.class)
                .addMappings(m->m.skip(Films::setDirectors)).setPostConverter(toEntityConverter());
    }
    @Override
    protected void mapSpecificFields(FilmsWithDirectorsDTO source, Films destination) {
        destination.setDirectors(new HashSet<>(directorsRepository.findAllById(source.getDirectorsId())));
    }

    @Override
    protected void mapSpecificFields(Films source, FilmsWithDirectorsDTO destination) {
        destination.setDirectorsId(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Films entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ?null
                : entity.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
