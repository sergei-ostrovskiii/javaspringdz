package ru.sber.homework.java13homework.library.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Setter
@Getter
@Table(name="directors")
@NoArgsConstructor
@SequenceGenerator(name = "default_Generator", sequenceName = "directors_seq", allocationSize = 1)
//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class Directors extends GenericModel{


    @Column(name = "directors_fio", nullable = false)
    private String directorsFIO;

    @Column(name="position")
    private String position;

//    @ManyToMany(mappedBy = "directors")
//    private Set<Films> films;
    @ManyToMany
    // @JsonIgnore
    //@JsonManagedReference
    @JoinTable(name="films_directors",
    joinColumns = @JoinColumn(name = "director_id"), foreignKey = @ForeignKey(name = "FC_DIRECTORS_FILMS"),
            inverseJoinColumns = @JoinColumn(name ="film_id"), inverseForeignKey = @ForeignKey(name = "FC_FILMS_DIRECTORS"))
    private Set<Films> films;

    public Directors(String director1, LocalDate now, String description1, Object o) {
    }
}
