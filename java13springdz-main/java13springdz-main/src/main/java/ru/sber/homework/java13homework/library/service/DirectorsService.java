package ru.sber.homework.java13homework.library.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.sber.homework.java13homework.library.constants.Errors;
import ru.sber.homework.java13homework.library.dto.AddFilmsDTO;
import ru.sber.homework.java13homework.library.dto.DirectorsDTO;
import ru.sber.homework.java13homework.library.exception.MyDeleteException;
import ru.sber.homework.java13homework.library.mapper.DirectorsMapper;
import ru.sber.homework.java13homework.library.model.Directors;
import ru.sber.homework.java13homework.library.model.Films;
import ru.sber.homework.java13homework.library.repository.DirectorsRepository;
import ru.sber.homework.java13homework.library.repository.FilmsRepository;
import ru.sber.homework.java13homework.library.repository.GenericRepository;

import java.util.List;
import java.util.Set;

@Service
public class DirectorsService extends GenericService<Directors, DirectorsDTO> {

    private final DirectorsRepository directorsRepository;
    private final FilmsService filmsService;

    protected DirectorsService(DirectorsRepository directorsRepository,
                            DirectorsMapper directorsMapper,
                            FilmsService filmsService) {
        super(directorsRepository, directorsMapper);
        this.directorsRepository = directorsRepository;
        this.filmsService = filmsService;
    }

    public Page<DirectorsDTO> searchDirectors(final String fio,
                                         Pageable pageable) {
        Page<Directors> directors = directorsRepository.findAllByDirectorsFIOContainsIgnoreCaseAndIsDeletedFalse(fio, pageable);    //findAllByDirectorsFIOContainsIgnoreCaseAndIsDeletedFalse(fio, pageable);
        List<DirectorsDTO> result = mapper.toDTOs(directors.getContent());
        return new PageImpl<>(result, pageable, directors.getTotalElements());
    }

    public void addFilms(AddFilmsDTO addFilmsDTO) {
        DirectorsDTO directors = getOne(addFilmsDTO.getDirectorsId());
        filmsService.getOne(addFilmsDTO.getFilmsId());
        directors.getFilmsId().add(addFilmsDTO.getFilmsId());
        update(directors);
    }

    @Override
    public void deleteSoft(Long objectId) throws MyDeleteException {
        Directors directors = directorsRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Продюсер с заданным id=" + objectId + " не существует."));
        boolean directorsCanBeDeleted = directorsRepository.checkDirectorsForDeletion(objectId);
        if (directorsCanBeDeleted) {
            markAsDeleted(directors);
            Set<Films> films = directors.getFilms();
            if (films != null && films.size() > 0) {
                films.forEach(this::markAsDeleted);
            }
            directorsRepository.save(directors);
        }
        else {
            throw new MyDeleteException(Errors.Authors.AUTHOR_DELETE_ERROR);
        }
    }

    public void restore(Long objectId) {
        Directors directors = directorsRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Продюсер с заданным id=" + objectId + " не существует."));
        unMarkAsDeleted(directors);
        Set<Films> films = directors.getFilms();
        if (films != null && films.size() > 0) {
            films.forEach(this::unMarkAsDeleted);
        }
        directorsRepository.save(directors);
    }
}
