package ru.sber.homework.java13homework.library.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.homework.java13homework.library.model.Films;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmsWithDirectorsDTO extends FilmsDTO {
    public FilmsWithDirectorsDTO(Films films, Set<DirectorsDTO> directors) {
        super(films);
        this.directors = directors;
    }
private Set<DirectorsDTO> directors;
}
