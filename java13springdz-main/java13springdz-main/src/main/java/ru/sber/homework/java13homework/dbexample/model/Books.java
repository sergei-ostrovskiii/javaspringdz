package ru.sber.homework.java13homework.dbexample.model;

import lombok.*;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Books {
    private Integer bookId;
    private String bookTitle;
    private String bookAuthor;
    private Date dateAdded;

}
