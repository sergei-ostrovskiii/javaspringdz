package ru.sber.homework.java13homework.library.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.sber.homework.java13homework.library.model.Directors;

@Repository
public interface DirectorsRepository extends GenericRepository<Directors> {
    Page<Directors> findAllByIsDeletedFalse(Pageable pageable);
//

    Page<Directors> findAllByDirectorsFIOContainsIgnoreCaseAndIsDeletedFalse(String fio, Pageable pageable);


    @Query(value = """
          select case when count(a) > 0 then false else true end
          from Directors a join a.films b
                        join Orders bri on b.id = bri.films.id
          where a.id = :directorsId
          """)

boolean checkDirectorsForDeletion(final Long directorsId);
}
